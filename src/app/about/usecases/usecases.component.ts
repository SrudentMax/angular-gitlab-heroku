import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.css']
})
export class UsecasesComponent implements OnInit {
  readonly NON_AUTH_USER = 'niet ingelogde gebruiker'
  readonly PLAIN_USER = 'Reguliere gebruiker'
  readonly ADMIN_USER = 'Administrator'

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd'
    },
    {
      id: 'UC-02',
      name: 'registreren',
      description: 'Hiermee kan een nieuwe gebruiker een account aanmaken',
      scenario: [
        'Gebruiker vult zijn/haar informatie in en klikt op registreer knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan maakt de applicatie een nieuw account aan',
        'daarna redirect de applicatie naar het startscherm.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'geen',
      postcondition: 'de nieuwe gebruiker heeft een account'
    },
    {
      id: 'UC-03',
      name: 'muziek toevoegen',
      description: 'Hiermee kan een gebruiker een nieuw nummer toevoegen. ',
      scenario: [
        'Gebruiker vult informatie over het nummer in',
        'de gebruiker drukt op toevoegen.',
        'De applicatie stuurt de data naar de database',
        'gebruikers die geabboneerd zijn krijgen een melding'
      ],
      actor: this.PLAIN_USER,
      precondition: 'de gebruiker is naar de nummer toevoegen pagina genavigeerd',
      postcondition: 'de gebruiker heeft een nieuw nummer toegevoegd'
    },
    {
      id: 'UC-04',
      name: 'muziek aanpassen',
      description: 'Hiermee kan een gebruiker de details van een nummer aanpassen. ',
      scenario: [
        'de gebruiker navigeerd naar zijn nummer',
        'de gebruiker drukt op de edit knop',
        'Gebruiker vult informatie over het nummer in'
      ],
      actor: this.PLAIN_USER,
      precondition: 'de gebruiker is ingelogd',
      postcondition: 'de gebruiker heeft een nummer aangepast'
    },
    {
      id: 'UC-05',
      name: 'muziek zoeken',
      description: 'Hiermee kan een gebruiker een nummer vinden. ',
      scenario: [
        'Gebruiker vult informatie over het nummer in',
        'de gebruiker drukt op zoeken.',
        'De applicatie geeft een lijst terug van alle nummers die voldoen aan de gegeven criteria '
      ],
      actor: this.NON_AUTH_USER,
      precondition: 'de gebruike is ingelogd',
      postcondition: 'de gebruiker heeft een lijst van nummers'
    },
    {
      id: 'UC-06',
      name: 'muziek delen',
      description: 'Hiermee kan een gebruiker een link verkijgen om het nummer te delen met vrienden',
      scenario: [
        'Gebruiker vult informatie over het nummer in',
        'de gebruiker drukt op delen',
        'de gebruiker ontvangt een popup met een deel link'
      ],
      actor: this.NON_AUTH_USER,
      precondition: 'geen',
      postcondition: 'de gebruiker heeft een link naar een specifiek nummer'
    },
    {
      id: 'UC-07',
      name: 'aanmaken van een afspeeltijst',
      description: 'Hiermee kan een gebruiker een afspeelijst maken om zijn muziek te groeperen',
      scenario: ['Gebruiker vult informatie over de afspeelijst in', 'de gebruiker drukt op aanmaken'],
      actor: this.PLAIN_USER,
      precondition: 'de gebruiker is ingelogd',
      postcondition: 'de gebruiker heeft een nieuwe afspeelijst gemaakt'
    },
    {
      id: 'UC-08',
      name: 'verwijderen van een afspeeltijst',
      description: 'Hiermee kan een gebruiker een afspeelijst verwijderen',
      scenario: ['Gebruiker drukt op de verwijder knop op een playlist', 'de gebruiker drukt op bevestigen'],
      actor: this.PLAIN_USER,
      precondition: 'de gebruiker is ingelogd & de gebruiker heeft een playlist',
      postcondition: 'de gebruiker heeft een afspeelijst verwijderd'
    },
    {
      id: 'UC-09',
      name: 'aanpassen van een afspeeltijst',
      description: 'Hiermee kan een gebruiker de naam en description van een playlist aanpassen.',
      scenario: [
        'Gebruiker drukt op de aanpassen knop op een playlist',
        'Gebruiker dpast de nodige data aan',
        'de gebruiker drukt op bevestigen'
      ],
      actor: this.PLAIN_USER,
      precondition: 'de gebruiker is ingelogd & de gebruiker heeft een playlist',
      postcondition: 'de gebruiker heeft een afspeelijst aangepast'
    },
    {
      id: 'UC-10',
      name: 'muziek toevoegen aan een afspeeltijst',
      description: 'Hiermee kan een gebruiker muziek toevoegen aan een playlist.',
      scenario: [
        'de gebruiker navigeerd naar een nummer.',
        'Gebruiker gebruikt tor knop toevoegen aan afspeelijst',
        'er word een lijst eggenereerd van afspeelijsten van een gebruiker',
        'de gebruiker selecteerd een afspeelijst ',
        'de gebruiker drukt op bevestigen'
      ],
      actor: this.PLAIN_USER,
      precondition: 'de gebruiker is ingelogd & de gebruiker heeft een playlist',
      postcondition: 'de gebruiker heeft een nummer toegevoegd aan een afspeelijst'
    },
    {
      id: 'UC-11',
      name: 'muziek verwijderen uit een afspeeltijst',
      description: 'Hiermee kan een gebruiker muziek verwijderen uit een playlist.',
      scenario: [
        'de gebruiker navigeerd naar een nummer in zijn of haar playlist.',
        'Gebruiker gebruikt de knop verwijderen ',
        'de gebruiker drukt op bevestigen'
      ],
      actor: this.PLAIN_USER,
      precondition: 'de gebruiker is ingelogd & de gebruiker heeft een playlist',
      postcondition: 'de gebruiker heeft een nummer verwijderd uit een afspeelijst'
    },
    {
      id: 'UC-12',
      name: 'het plaatsen van een comment',
      description: 'Hiermee kan een gebruiker zijn mening delen over een nummer ',
      scenario: [
        'de gebruiker navigeerd naar een nummer ',
        'de gebruiker typt zijn mening uit over het desbetreffende nummer in het comment vak',
        'de gebruiker drukt op plaatsen'
      ],
      actor: this.PLAIN_USER,
      precondition: 'de gebruiker is ingelogd',
      postcondition: 'de gebruiker heeft commentaar toegevoegd op een nummer'
    },
    {
      id: 'UC-13',
      name: 'het plaatsen van een comment',
      description: 'Hiermee kan een gebruiker zijn mening delen over een nummer ',
      scenario: [
        'de gebruiker navigeerd naar een nummer ',
        'de gebruiker typt zijn mening uit over het desbetreffende nummer in het comment vak',
        'de gebruiker drukt op plaatsen'
      ],
      actor: this.PLAIN_USER,
      precondition: 'de gebruiker is ingelogd',
      postcondition: 'de gebruiker heeft commentaar toegevoegd op een nummer'
    }
  ]

  constructor() {}

  ngOnInit(): void {}
}
